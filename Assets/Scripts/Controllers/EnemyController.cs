﻿using Engine.Common.Events;
using Engine.Components;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {
    [SerializeField]
    private GameObject _enemyPrefab;
    [SerializeField]
    private List<Transform> _spawnPoints;

    private List<LevelObject> _spawnedEnemy;
    private Coroutine _spawner;

    private void Start() {
        _spawnedEnemy = new List<LevelObject>();
        _spawner = StartCoroutine(Spawner());
        EventManager.Instance.AddEventListener<LevelObjectEvent>(OnLevelObjectEvent);
        EventManager.Instance.AddEventListener<EnemyAIEvent>(OnEnemyAIEvent);
    }

    private void OnEnemyAIEvent(EnemyAIEvent enemyAIEvent) {
        if (enemyAIEvent.EventSubtype != EnemyAIEventType.RunAway) return;
        if (!_spawnedEnemy.Contains(enemyAIEvent.Target.LevelObject)) return;
        StartCoroutine(DestroyEnemy(enemyAIEvent.Target.gameObject));
    }

    private void OnLevelObjectEvent(LevelObjectEvent levelObjectEvent) {
        if (levelObjectEvent.EventSubtype != LevelObjectEventType.Dead) return;
        if (!_spawnedEnemy.Contains(levelObjectEvent.Target)) return;
        StartCoroutine(DestroyEnemy(levelObjectEvent.Target.gameObject));
    }

    private IEnumerator DestroyEnemy(GameObject enemy) {
        //Буду честен, это костыль...
        yield return new WaitForSeconds(1);
        Destroy(enemy);
    }

    private void OnDisable() {
        if (_spawner != null)
            StopCoroutine(_spawner);
    }

    private IEnumerator Spawner() {
        //Да и спавн врагов надо бы через пул сделать
        var delay = new WaitForSeconds(2f);
        while (true) {
            var randomTransform = GetRandomPosition();
            var enemy = Instantiate(_enemyPrefab, randomTransform.position, randomTransform.rotation * Quaternion.Euler(GetRandomRotation()));
            _spawnedEnemy.Add(enemy.GetComponent<LevelObject>());
            yield return delay;
        }
    }

    private Transform GetRandomPosition() {
        return _spawnPoints[Random.Range(0, _spawnPoints.Count)];
    }

    private Vector3 GetRandomRotation() {
        return new Vector3(0, 0, Random.Range(-60, 60));
    }
}
