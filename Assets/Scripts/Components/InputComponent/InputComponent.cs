﻿using Engine.Common.Events;
using UnityEngine;

namespace Engine.Components {
    public class InputComponent : BaseComponent {
        private RunnerComponent _runnerComponent;

        protected override void Init() {
            _runnerComponent = GetComponent<RunnerComponent>();
        }

        private void Update() {
            var moveVelocity = new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"), 0);
            var rotation = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            float rotation_y = Mathf.Atan2(rotation.x, rotation.y) * Mathf.Rad2Deg;
            _runnerComponent.SetDirection(moveVelocity, -rotation_y);

            if (Input.GetMouseButtonDown(0)) {
                EventManager.Instance.DispatchEvent(new InputComponentEvent(InputComponentEventType.Shoot, this));
            }
        }
    }
}
